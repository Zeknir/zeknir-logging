﻿namespace Zeknir.Logging
{
	/// <summary>
	/// Defined the importance of a Log Entry.
	/// </summary>
	public enum LogLevel
	{
		/// <summary>
		/// Marks entries for debugging.
		/// </summary>
		Debug,
		/// <summary>
		/// Marks entries for general information.
		/// </summary>
		Info,
		/// <summary>
		/// Marks entries for warnings.
		/// </summary>
		Warning,
		/// <summary>
		/// Marks entries for recoverable errors.
		/// </summary>
		Error,
		/// <summary>
		/// Marks entries for unrecoverable errors / crashes.
		/// </summary>
		Fatal
	}
}
