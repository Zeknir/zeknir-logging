﻿namespace Zeknir.Logging.Entries
{
    /// <summary>
    /// Defines a container with meta data for a <see cref="LogEntry"/> instance.
    /// </summary>
    public interface ILogEntryMetaData { }
}