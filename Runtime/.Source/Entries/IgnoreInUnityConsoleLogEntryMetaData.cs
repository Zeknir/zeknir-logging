﻿namespace Zeknir.Logging.Entries
{
    /// <summary>
    /// Represents a flag as <see cref="ILogEntryMetaData"/> that the log entry should not be emitted into the Unity Console.
    /// </summary>
    public struct IgnoreInUnityConsoleLogEntryMetaData : ILogEntryMetaData { }
}