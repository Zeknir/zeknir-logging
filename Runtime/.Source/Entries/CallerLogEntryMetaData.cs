﻿namespace Zeknir.Logging.Entries
{
    /// <summary>
    /// Provides the caller of the log method as meta data.
    /// </summary>
    public readonly struct CallerLogEntryMetaData : ILogEntryMetaData
    {
        /// <summary>
        /// The caller of the log method.
        /// </summary>
        public readonly string Caller;

        /// <summary>
        /// Creates a new <see cref="CallerLogEntryMetaData"/> instance.
        /// </summary>
        /// <param name="caller">The caller of the log method.</param>
        public CallerLogEntryMetaData(string caller)
        {
            this.Caller = caller;
        }
    }
}