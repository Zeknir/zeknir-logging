﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zeknir.Logging.Entries
{
    /// <summary>
    /// Represents an entry to a log.
    /// </summary>
    public readonly struct LogEntry
    {
        /// <summary>
        /// The message of the entry.
        /// </summary>
        public readonly FormattableString Message;
        /// <summary>
        /// The importance level of the entry.
        /// </summary>
        public readonly LogLevel Level;
        /// <summary>
        /// The timestamp of when the entry was created.
        /// </summary>
        public readonly DateTime Timestamp;
        
        private readonly Dictionary<Type, ILogEntryMetaData>? metaData;

        /// <summary>
        /// Creates a new <see cref="LogEntry"/> instance.
        /// </summary>
        /// <param name="message">The message of the entry.</param>
        /// <param name="level">The importance level of the entry.</param>
        /// <param name="timestamp">The timestamp of when the entry was created.</param>
        /// <param name="metaData">The metadata to be associated with this entry. Only one entry per <see cref="Type"/> is allowed.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="message"/> is <see langword="null"/>.</exception>
        public LogEntry(FormattableString message, LogLevel level, DateTime timestamp, IEnumerable<ILogEntryMetaData>? metaData = null)
        {
            this.Message = message ?? throw new ArgumentNullException(nameof(message));
            this.Level = level;
            this.Timestamp = timestamp;
            this.metaData = metaData?.ToDictionary(a => a.GetType(), a => a);
        }

        /// <summary>
        /// Tries to get the <see cref="ILogEntryMetaData"/> instance for this entry by the type.
        /// </summary>
        /// <param name="value">The <see cref="ILogEntryMetaData"/> instance associated with this entry. Is only valid when the method returns <see langword="true"/>.</param>
        /// <typeparam name="T">The type of the <see cref="ILogEntryMetaData"/> to be gathered.</typeparam>
        /// <returns>Whether the requested <see cref="ILogEntryMetaData"/> was found.</returns>
        public bool TryGetMetaData<T>(out T value) where T : ILogEntryMetaData
        {
            if (this.metaData != null && this.metaData.TryGetValue(typeof(T), out var data))
            {
                value = (T) data;
                return true;
            }

            value = default!;
            return false;
        }
    }
}