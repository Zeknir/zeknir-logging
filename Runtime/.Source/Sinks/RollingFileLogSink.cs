﻿using System;
using System.IO;
using System.Linq;
using Zeknir.Logging.Formatter;

namespace Zeknir.Logging.Sinks
{
    /// <summary>
    /// A <see cref="ILogSink"/> which writes into a rolling plain-text file onto the drive.
    /// </summary>
    public sealed class RollingFileLogSink : TextLogSink
    {
        private readonly StreamWriter logFileWriter;

        /// <summary>
        /// The file that's currently written to.
        /// </summary>
        public string CurrentFile { get; }

        /// <summary>
        /// Creates a new <see cref="RollingFileLogSink"/> instance.
        /// </summary>
        /// <param name="formatter">The <see cref="ILogFormatter"/> object to use for emitting.</param>
        /// <param name="settings">The settings for the rolling output file.</param>
        public RollingFileLogSink(ILogFormatter formatter, RollingFileSettings settings) : base(formatter)
        {
            // Ensure Folder
            var folder = new DirectoryInfo(settings.FolderPath);
            if (!folder.Exists)
                folder.Create();

            // Delete old Files
            var existingFiles = folder.GetFiles().Where(a => a.Name.StartsWith(settings.FileName)).OrderBy(f => f.LastWriteTime).ToList();
            var filesToDelete = Math.Max(0, existingFiles.Count - (settings.NumberOfFilesToKeep - 1));

            for (var i = 0; i < filesToDelete; i++)
                existingFiles[i].Delete();

            // Create File
            var newFileName = $"{settings.FileName}{DateTime.Now.ToString(settings.DateTimeFormat)}.log";
            var newFilePath = Path.Combine(settings.FolderPath, newFileName);

            var fileStream = File.Open(newFilePath, FileMode.Create, FileAccess.Write, FileShare.Read);
            this.logFileWriter = new StreamWriter(fileStream);
            this.CurrentFile = newFilePath;
        }

        /// <inheritdoc />
        protected override void Emit(string entry)
        {
            this.logFileWriter.WriteLine(entry);
            this.logFileWriter.Flush();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            this.logFileWriter.Flush();
            this.logFileWriter.Dispose();
        }
    }

    /// <summary>
    /// Provides settings for a rolling file manager.
    /// </summary>
    public readonly struct RollingFileSettings
    {
        /// <summary>
        /// The path of the folder to store the files to.
        /// </summary>
        public readonly string FolderPath;

        /// <summary>
        /// The base name of the files.
        /// </summary>
        public readonly string FileName;

        /// <summary>
        /// The format of the timestamp appended to the filename.
        /// </summary>
        public readonly string DateTimeFormat;

        /// <summary>
        /// The number of older files to keep when creating a new one.
        /// </summary>
        public readonly int NumberOfFilesToKeep;

        /// <summary>
        /// Creates a new <see cref="RollingFileSettings"/> instance.
        /// </summary>
        /// <param name="folderPath">The path of the folder to store the files to.</param>
        /// <param name="fileName">The base name of the files.</param>
        /// <param name="dateTimeFormat">The format of the timestamp appended to the filename.</param>
        /// <param name="numberOfFilesToKeep">The number of older files to keep when creating a new one.</param>
        public RollingFileSettings(string folderPath, string fileName, string dateTimeFormat = "yyyy-MM-dd_HH-mm-ss", int numberOfFilesToKeep = 3)
        {
            this.FolderPath = folderPath;
            this.FileName = fileName;
            this.DateTimeFormat = dateTimeFormat;
            this.NumberOfFilesToKeep = numberOfFilesToKeep;
        }
    }
}