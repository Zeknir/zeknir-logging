﻿using System;
using Zeknir.Logging.Entries;
using Zeknir.Logging.Formatter;

namespace Zeknir.Logging.Sinks
{
    /// <summary>
    /// Emits <see cref="LogEntry"/> objects into a subscribable event.
    /// </summary>
    public class EventLogSink : TextLogSink
    {
        /// <summary>
        /// Occurs when a <see cref="LogEntry"/> is emitted by this sink.
        /// </summary>
        public event Action<EventLogSinkEventArguments>? Emitted;

        /// <summary>
        /// Creates a new <see cref="EventLogSink"/>
        /// </summary>
        /// <param name="formatter">The <see cref="ILogFormatter"/> object to use for emitting.</param>
        public EventLogSink(ILogFormatter formatter) : base(formatter) { }

        /// <inheritdoc />
        public override void Emit(LogEntry entry)
        {
            this.Emitted?.Invoke(new EventLogSinkEventArguments(entry, this.Formatter.Format(entry)));
        }
    }

    /// <summary>
    /// Provides event arguments for an <see cref="EventLogSink.Emitted"/> event.
    /// </summary>
    public readonly struct EventLogSinkEventArguments
    {
        /// <summary>
        /// The <see cref="LogEntry"/> object emitted.
        /// </summary>
        public readonly LogEntry LogEntry;
        /// <summary>
        /// The <see cref="LogEntry"/> as formatted string.
        /// </summary>
        public readonly string FormattedLogEntry;

        /// <summary>
        /// Creates a new <see cref="EventLogSinkEventArguments"/> instance.
        /// </summary>
        /// <param name="logEntry">The <see cref="LogEntry"/>.</param>
        /// <param name="formattedLogEntry">The <see cref="LogEntry"/> as formatted <see cref="string"/>.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="formattedLogEntry"/> is <see langword="null"/>.</exception>
        public EventLogSinkEventArguments(LogEntry logEntry, string formattedLogEntry)
        {
            this.LogEntry = logEntry;
            this.FormattedLogEntry = formattedLogEntry ?? throw new ArgumentNullException(nameof(formattedLogEntry));
        }
    }
}