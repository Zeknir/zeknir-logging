﻿using Zeknir.Logging.Entries;
using Zeknir.Logging.Formatter;

namespace Zeknir.Logging.Sinks
{
    /// <summary>
    /// A base <see cref="ILogSink"/> for emitting formattable text-based outputs using a <see cref="ILogFormatter"/>.
    /// </summary>
    public abstract class TextLogSink : ILogSink
    {
        /// <summary>
        /// The <see cref="ILogFormatter"/> used for emitting.
        /// </summary>
        public ILogFormatter Formatter { get; }

        /// <summary>
        /// Creates a new <see cref="TextLogSink"/>.
        /// </summary>
        /// <param name="formatter">The <see cref="ILogFormatter"/> object to use for emitting.</param>
        protected TextLogSink(ILogFormatter formatter)
        {
            this.Formatter = formatter;
        }

        /// <inheritdoc/>
        public virtual void Emit(LogEntry entry)
        {
            this.Emit(this.Formatter.Format(entry));
        }

        /// <summary>
        /// Emits a pre-formatted <see cref="string"/> into the target. 
        /// </summary>
        /// <param name="entry">The <see cref="string"/> to emit.</param>
        protected virtual void Emit(string entry) { }

        /// <inheritdoc/>
        public virtual void Dispose() { }
    }
}