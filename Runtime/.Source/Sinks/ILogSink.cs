﻿using System;
using Zeknir.Logging.Entries;

namespace Zeknir.Logging.Sinks
{
	/// <summary>
	/// Represents an output point for <see cref="LogEntry"/> objects.
	/// </summary>
	public interface ILogSink : IDisposable
	{
		/// <summary>
		/// Emits the <see cref="LogEntry"/> into the target.
		/// </summary>
		/// <param name="entry">The entry to emit.</param>
		void Emit(LogEntry entry);
	}
}
