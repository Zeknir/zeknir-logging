﻿using UnityEngine;
using Zeknir.Logging.Entries;
using Zeknir.Logging.Formatter;

namespace Zeknir.Logging.Sinks
{
    /// <summary>
    /// A <see cref="ILogSink"/> for emitting into the Unity Console.
    /// </summary>
    public sealed class UnityConsoleLogSink : ILogSink
    {
        private readonly ILogFormatter formatter = new UnityConsoleLogFormatter();

        /// <inheritdoc />
        public void Emit(LogEntry entry)
        {
            if (entry.TryGetMetaData<IgnoreInUnityConsoleLogEntryMetaData>(out _))
                return;

            var text = this.formatter.Format(entry);

            switch (entry.Level)
            {
                case LogLevel.Debug:
                    Debug.Log(text);
                    break;
                case LogLevel.Info:
                    Debug.Log(text);
                    break;
                case LogLevel.Warning:
                    Debug.LogWarning(text);
                    break;
                case LogLevel.Error:
                    Debug.LogError(text);
                    break;
                case LogLevel.Fatal:
                    Debug.LogError(text);
                    break;
            }
        }

        /// <inheritdoc />
        public void Dispose() { }
    }
}