﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zeknir.Logging.Entries;

namespace Zeknir.Logging.Sinks
{
    /// <summary>
    /// A <see cref="ILogSink"/> which filters <see cref="LogEntry"/> objects with a <see cref="Predicate{T}"/> and emits matching ones into other <see cref="ILogSink"/> objects.
    /// </summary>
    public class FilteredLogSink : ILogSink
    {
        private readonly ILogSink[] targetSinks;
        private readonly Predicate<LogEntry> filter;

        /// <summary>
        /// Creates a new <see cref="FilteredLogSink"/>.
        /// </summary>
        /// <param name="targetSinks">The sinks to emit matching <see cref="LogEntry"/> objects to.</param>
        /// <param name="filter">The <see cref="Predicate{T}"/> defining the filter. Return <see langword="true"/> to emit, and <see langword="false"/> to ignore a <see cref="LogEntry"/> object.</param>
        public FilteredLogSink(IEnumerable<ILogSink> targetSinks, Predicate<LogEntry> filter)
        {
            this.targetSinks = targetSinks.ToArray();
            this.filter = filter;
        }

        ///<inheritdoc/>
        public void Emit(LogEntry entry)
        {
            if (!this.filter(entry))
                return;

            foreach (var targetSink in this.targetSinks)
                targetSink.Emit(entry);
        }

        /// <summary>
        /// Disposes this object and all attached <see cref="ILogSink"/> objects.
        /// </summary>
        public void Dispose()
        {
            foreach (var targetSink in this.targetSinks)
                targetSink.Dispose();
        }
    }
}