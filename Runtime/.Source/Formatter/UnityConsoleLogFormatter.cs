﻿using System;
using System.Globalization;
using UnityEngine;
using Zeknir.Logging.Entries;
using Zeknir.Logging.Unity;

namespace Zeknir.Logging.Formatter
{
    /// <summary>
    /// A <see cref="ILogFormatter"/> for pretty Unity Console output.
    /// </summary>
    public class UnityConsoleLogFormatter : ILogFormatter
    {
        /// <inheritdoc />
        public string Format(LogEntry entry)
        {
            var infoString = $"{entry.Timestamp:HH:mm:ss}";
            var infoColor = new Color(0.2f, 0.5f, 1);

            if (entry.TryGetMetaData<CallerLogEntryMetaData>(out var caller))
            {
                var callerHash = (uint) caller.Caller.GetHashCode();
                var hueInt = callerHash & 0x0000_FFFF;
                var lightnessInt = (callerHash >> 16) & 0x0000_FFFF;
                var hue = hueInt / (float) 0xFFFF;
                var lightness = lightnessInt / (float) 0xFFFF * .4f + .3f;
                infoColor = UnityColorHelper.FromHsl(hue, 0.9f, lightness);
                infoString += $"|{caller.Caller}";
            }

            var message = entry.Message.ToString(new UnityConsoleFormatProvider());

            return $"<color=#{infoColor.ToHex()}>[{infoString}]</color> {message}";
        }

        private class UnityConsoleFormatProvider : IFormatProvider, ICustomFormatter
        {
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string text;

                if (arg is IFormattable formattable)
                    text = formattable.ToString(format, CultureInfo.InvariantCulture);
                else
                    text = arg.ToString();

                Color color;
                switch (arg)
                {
                    case int _:
                    case float _:
                    case decimal _:
                    case long _:
                    case double _:
                        color = UnityColorHelper.FromHsl(220, 0.65f, 0.55f);
                        break;
                    case string _:
                        color = UnityColorHelper.FromHsl(027, 0.55f, 0.45f);
                        break;
                    case Exception _:
                        color = UnityColorHelper.FromHsl(000, 1.00f, 0.65f);
                        break;
                    case UnityEngine.Object _:
                        color = UnityColorHelper.FromHsl(280, 0.50f, 0.60f);
                        break;
                    default:
                        if (arg.GetType().IsValueType)
                            color = UnityColorHelper.FromHsl(070, 0.80f, 0.30f);
                        else if (arg.GetType().IsEnum)
                            color = UnityColorHelper.FromHsl(140, 0.50f, 0.40f);
                        else
                            color = UnityColorHelper.FromHsl(200, 0.60f, 0.40f);
                        break;
                }

                return $"<b><color=#{color.ToHex()}>{text}</color></b>";
            }

            public object? GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter))
                    return this;
                else
                    return null;
            }
        }
    }
}