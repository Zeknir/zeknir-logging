﻿using System.Globalization;
using Zeknir.Logging.Entries;

namespace Zeknir.Logging.Formatter
{
    /// <summary>
    /// Provides a standard <see cref="ILogFormatter"/> which follows the following format: <c>[timestamp|level|caller] message</c>
    /// </summary>
    public class StandardLogFormatter : ILogFormatter
    {
        /// <summary>
        /// The format of the timestamp for the output.
        /// </summary>
        public readonly string TimeFormat;

        /// <summary>
        /// Creates a new <see cref="StandardLogFormatter"/>.
        /// </summary>
        /// <param name="timeFormat">The optional timestamp format.</param>
        public StandardLogFormatter(string timeFormat = "HH:mm:ss")
        {
            this.TimeFormat = timeFormat;
        }

        /// <inheritdoc/>
        public string Format(LogEntry entry)
        {
            var infoString = $"{entry.Timestamp.ToString(this.TimeFormat)}|{entry.Level.ToString()}";

            if (entry.TryGetMetaData<CallerLogEntryMetaData>(out var caller))
                infoString += $"|{caller}";

            return $"[{infoString}] {entry.Message.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}