﻿using Zeknir.Logging.Entries;

namespace Zeknir.Logging.Formatter
{
    /// <summary>
    /// Provides a method to format a <see cref="LogEntry"/> into a <see cref="string"/>.
    /// </summary>
    public interface ILogFormatter
    {
        /// <summary>
        /// Formats a <see cref="LogEntry"/> into a <see cref="string"/>.
        /// </summary>
        /// <param name="entry">The <see cref="LogEntry"/> to format.</param>
        /// <returns>The representative <see cref="string"/>.</returns>
        string Format(LogEntry entry);
    }
}