﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Zeknir.Logging.Entries;

namespace Zeknir.Logging
{
    /// <summary>
    /// Provides a way to store a <see cref="Logger"/> object globally and access it directly with static methods. 
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// The current global <see cref="Zeknir.Logging.Logger"/> object.
        /// </summary>
        public static Logger? Logger { get; set; }

        /// <summary>
        /// Writes a log message with <see cref="Logger"/>.
        /// </summary>
        /// <param name="message">The formattable message to write.</param>
        /// <param name="logLevel">The log level of the message.</param>
        /// <param name="metaData">An optional collection of additional metadata.</param>
        /// <param name="sourceFilePath">The caller filepath.</param>
        /// <exception cref="InvalidOperationException">Thrown when no <see cref="Zeknir.Logging.Logger"/> object is in <see cref="Logger"/>.</exception>
        public static void Write(FormattableString message, LogLevel logLevel = LogLevel.Info, IEnumerable<ILogEntryMetaData>? metaData = null, [CallerFilePath] string sourceFilePath = "")
        {
            if (Logger == null)
                throw new InvalidOperationException("No logger has been set.");

            // ReSharper disable once ExplicitCallerInfoArgument
            Logger.Write(message, logLevel, metaData, sourceFilePath);
        }

        /// <summary>
        /// Writes a log message with <see cref="Logger"/>.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <param name="logLevel">The log level of the message.</param>
        /// <param name="metaData">An optional collection of additional metadata.</param>
        /// <param name="sourceFilePath">The caller filepath.</param>
        /// <exception cref="InvalidOperationException">Thrown when no <see cref="Zeknir.Logging.Logger"/> object is in <see cref="Logger"/>.</exception>
        public static void Write(object message, LogLevel logLevel = LogLevel.Info, IEnumerable<ILogEntryMetaData>? metaData = null, [CallerFilePath] string sourceFilePath = "")
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            Write((FormattableString) $"{message}", logLevel, metaData, sourceFilePath);
        }

        /// <summary>
        /// Writes a log message with <see cref="Logger"/>.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <param name="logLevel">The log level of the message.</param>
        /// <param name="metaData">An optional collection of additional metadata.</param>
        /// <param name="sourceFilePath">The caller filepath.</param>
        /// <exception cref="InvalidOperationException">Thrown when no <see cref="Zeknir.Logging.Logger"/> object is in <see cref="Logger"/>.</exception>
        public static void Write(string message, LogLevel logLevel = LogLevel.Info, IEnumerable<ILogEntryMetaData>? metaData = null, [CallerFilePath] string sourceFilePath = "")
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            Write(FormattableStringFactory.Create(message), logLevel, metaData, sourceFilePath);
        }
    }
}