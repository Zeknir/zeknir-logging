﻿using UnityEngine;

namespace Zeknir.Logging.Unity
{
	internal static class UnityColorHelper
	{
		internal static string ToHex(this Color c)
		{
			return rgbToInt(c.r).ToString("X2") + rgbToInt(c.g).ToString("X2") + rgbToInt(c.b).ToString("X2");

			int rgbToInt(float value)
			{
				return Mathf.RoundToInt(value * 255);
			}
		}

		internal static Color FromHsl(int hue, float saturation, float lightness)
		{
			return FromHsl(hue / 360f, saturation, lightness);
		}

		internal static Color FromHsl(float hue, float saturation, float lightness)
		{
			float r, g, b;

			if (saturation == 0.0f)
				r = g = b = lightness;

			else
			{
				var q = lightness < 0.5f ? lightness * (1.0f + saturation) : lightness + saturation - lightness * saturation;
				var p = 2.0f * lightness - q;
				r = hueToRgb(p, q, hue + 1.0f / 3.0f);
				g = hueToRgb(p, q, hue);
				b = hueToRgb(p, q, hue - 1.0f / 3.0f);
			}

			return new Color(r, g, b, 1.0f);

			// Helper for HslToRgba
			float hueToRgb(float p, float q, float t)
			{
				if (t < 0.0f) t += 1.0f;
				if (t > 1.0f) t -= 1.0f;
				if (t < 1.0f / 6.0f) return p + (q - p) * 6.0f * t;
				if (t < 1.0f / 2.0f) return q;
				if (t < 2.0f / 3.0f) return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
				return p;
			}
		}
	}
}
