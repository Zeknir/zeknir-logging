﻿using System;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace Zeknir.Logging.Unity
{
    /// <summary>
    /// Provides a method to intercept the Unity Log.
    /// </summary>
    public static class UnityLogHijacker
    {
        /// <summary>
        /// Occurs when an exception was logged in the Unity Log.
        /// </summary>
        public static event Action<UnhandledExceptionEventArgs>? UnhandledExceptionThrown;

        /// <summary>
        /// Injects a custom log handler into the Unity Log to intercept logs. The Unity Log will still function as normal.
        /// </summary>
        public static void HijackUnityLog()
        {
            var customLogger = new UnityLogHijackHandler(Debug.unityLogger.logHandler);
            Debug.unityLogger.logHandler = customLogger;
        }

        private class UnityLogHijackHandler : ILogHandler
        {
            private ILogHandler unityLogHandler;

            public UnityLogHijackHandler(ILogHandler logHandler)
            {
                this.unityLogHandler = logHandler;
            }

            public void LogException(Exception exception, UObject context)
            {
                UnhandledExceptionThrown?.Invoke(new UnhandledExceptionEventArgs(exception, context));

                this.unityLogHandler.LogException(exception, context);
            }

            public void LogFormat(LogType logType, UObject context, string format, params object[] args)
            {
                this.unityLogHandler.LogFormat(logType, context, format, args);
            }
        }
    }

    /// <summary>
    /// Contains event arguments for the <see cref="UnityLogHijacker.UnhandledExceptionThrown"/> event.
    /// </summary>
    public class UnhandledExceptionEventArgs
    {
        /// <summary>
        /// The thrown <see cref="Exception"/> object.
        /// </summary>
        public readonly Exception Exception;

        /// <summary>
        /// The optional context.
        /// </summary>
        public readonly UObject? Context;

        /// <summary>
        /// Creates a new <see cref="UnhandledExceptionEventArgs"/> instance.
        /// </summary>
        /// <param name="exception">The thrown <see cref="Exception"/>.</param>
        /// <param name="context">The optional context.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="exception"/> is <see langword="null"/>.</exception>
        public UnhandledExceptionEventArgs(Exception exception, UObject? context)
        {
            this.Exception = exception ?? throw new ArgumentNullException(nameof(exception));
            this.Context = context;
        }
    }
}