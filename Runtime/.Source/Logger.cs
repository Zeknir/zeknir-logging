﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Zeknir.Logging.Entries;
using Zeknir.Logging.Sinks;

namespace Zeknir.Logging
{
    /// <summary>
    /// Provides methods to easily create <see cref="LogEntry"/> objects and writing them into a collection of <see cref="ILogSink"/> objects.  
    /// </summary>
    public class Logger : IDisposable
    {
        /// <summary>
        /// Contains all sinks to write to.
        /// </summary>
        public HashSet<ILogSink> Sinks { get; } = new HashSet<ILogSink>();

        /// <summary>
        /// Creates a new <see cref="Logger"/> object.
        /// </summary>
        /// <param name="sinks">A list of sinks to write to.</param>
        public Logger(params ILogSink[] sinks)
        {
            foreach (var sink in sinks)
                this.Sinks.Add(sink);
        }
        
        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="message">The formattable message to write.</param>
        /// <param name="logLevel">The log level of the message.</param>
        /// <param name="metaData">An optional collection of additional metadata.</param>
        /// <param name="sourceFilePath">The caller filepath.</param>
        public void Write(FormattableString message, LogLevel logLevel = LogLevel.Info, IEnumerable<ILogEntryMetaData>? metaData = null, [CallerFilePath] string sourceFilePath = "")
        {
            metaData ??= Enumerable.Empty<ILogEntryMetaData>();
            
            var entry = new LogEntry(message,
                logLevel,
                DateTime.Now,
                metaData.Append(new CallerLogEntryMetaData(Path.GetFileNameWithoutExtension(sourceFilePath))));

            this.sendToSinks(entry);
        }

        private void sendToSinks(LogEntry entry)
        {
            foreach (var sink in this.Sinks)
                sink.Emit(entry);
        }

        /// <summary>
        /// Disposes this object and all <see cref="ILogSink"/> objects attached.
        /// </summary>
        public void Dispose()
        {
            foreach (var sink in this.Sinks)
                sink.Dispose();
        }
    }
}